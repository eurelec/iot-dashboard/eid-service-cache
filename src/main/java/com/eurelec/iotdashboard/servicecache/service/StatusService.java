package com.eurelec.iotdashboard.servicecache.service;

import com.eurelec.iotdashboard.servicecache.entity.Status;
import com.eurelec.iotdashboard.servicecache.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StatusService {

    @Autowired
    StatusRepository repository;

    public Optional<Status> findStatusByDeviceId(String deviceId){
        return repository.findById(deviceId);
    }
    public Status saveStatus(Status status){
        return repository.save(status);
    }
}
