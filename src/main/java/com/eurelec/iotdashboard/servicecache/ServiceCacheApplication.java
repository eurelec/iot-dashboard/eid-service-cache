package com.eurelec.iotdashboard.servicecache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootApplication
public class ServiceCacheApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceCacheApplication.class, args);
	}

}
