package com.eurelec.iotdashboard.servicecache.api.v1;

import com.eurelec.iotdashboard.servicecache.entity.Status;
import com.eurelec.iotdashboard.servicecache.repository.StatusRepository;
import com.eurelec.iotdashboard.servicecache.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/status")
public class StatusController {

    @Autowired
    StatusService statusService;

    @GetMapping("/{deviceId}")
    public ResponseEntity<Status> getValueByKey(@PathVariable String deviceId){
        var res = statusService.findStatusByDeviceId(deviceId);
        return res.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }
    @PutMapping()
    public ResponseEntity<Status> putValueByKey(@RequestBody Status status){
        return ResponseEntity.ok(statusService.saveStatus(status));
    }
}
