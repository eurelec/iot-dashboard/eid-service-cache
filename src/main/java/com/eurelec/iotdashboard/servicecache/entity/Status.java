package com.eurelec.iotdashboard.servicecache.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("status")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Status {
    @Id
    private String deviceId;
    private Boolean isOn = false;
}
