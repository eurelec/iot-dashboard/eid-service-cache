package com.eurelec.iotdashboard.servicecache.repository;

import com.eurelec.iotdashboard.servicecache.entity.Status;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends CrudRepository<Status, String> {
}
